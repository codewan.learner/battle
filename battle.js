var p1Button = document.querySelector("#p1Att");
var p2Button = document.getElementById("p2Att");
var resetButton = document.getElementById("resetButton");
var p1Display = document.querySelector("#p1Display");
var p2Display = document.querySelector("#p2Display");
var p1HP = 10;
var p2HP = 10;
var damRoll = 0;
var gameOver = false;

p1Button.addEventListener("click", function () {
    playerOneAttack();
});

p2Button.addEventListener("click", function () {
    playerTwoAttack();
});

resetButton.addEventListener("click", function () {
    reset();
});

function playerOneAttack() {
    if (!gameOver) {
        //determine if p1 hits p2 and determine damage
        damRoll = attackResult();
        console.log("p2HP: " + p2HP);
        console.log("damRoll: " + damRoll);
        p2HP = p2HP - damRoll;
        console.log("new p2HP: " + p2HP);
        //figure out if p1 defeated p2 or not
        if (p2HP <= 0) {
            p2Display.classList.add("loser");
            gameOver = true;
        }
        //change hp display for p2
        p2Display.textContent = p2HP;
    }
}

function playerTwoAttack() {
    if (!gameOver) {
        //determine if p2 hits p1 and determine damage
        damRoll = attackResult();
        console.log("p1HP: " + p1HP);
        console.log("damRoll: " + damRoll);
        p1HP = p1HP - damRoll;
        console.log("new p1HP: " + p1HP);
        //figure out if p1 defeated p2 or not
        if (p1HP <= 0) {
            p1Display.classList.add("loser");
            gameOver = true;
        }
        //change hp display for p2
        p1Display.textContent = p1HP;
    }
}

function reset() {
    p1HP = 10;
    p2HP = 10;
    p1Display.textContent = p1HP;
    p2Display.textContent = p2HP;
    p1Display.classList.remove("loser");
    p2Display.classList.remove("loser");
    gameOver = false;
}

function attackResult() {
    var attRoll = Math.floor(Math.random() * 6) + 1;
    if (attRoll < 4) {
        // console.log("You Hit with " + attRoll);
        var damRoll = Math.floor(Math.random() * 2) + 1;
    } else {
        // console.log("You Missed with " + attRoll);
        var damRoll = 0;
    }
    return damRoll;
}